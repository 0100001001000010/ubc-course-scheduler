import os
import traceback


def save(fileName, contents):
    """ Save some text at the given path, creating parent directories if necessary

    Parameters:
        fileName (str): The path to the file. Parent directories will be created if they do not yet exist

        contents (txt): The text that will go into the file
    """

    if os.path.dirname(fileName) != '':
        try:
            os.makedirs(os.path.dirname(fileName))
        except FileExistsError:
            pass
    with open(fileName, 'w') as file:
        file.write(contents)


def unknownError(exception):
    """Print an error message when an unknown exception occurs.

    Parameters:
        exception (exception): The exception that was thrown
    """

    print(
        f'An unknown error occured: {str(exception)}')
    print(
        'This might indicate a bug in the program. If so, please report it at https://gitlab.com/0100001001000010/ubc-course-scheduler/-/issues. The following stack trace might be useful:')
    traceback.print_exception(
        type(exception), exception, exception.__traceback__)
    raise SystemExit(1)


def promptYesNo(question):
    """Prompt the user for a yes or no answer

    Ask the user a yes or no question, appending "(y/n)" to make it more obvious that it is a yes or no question. Repeatedly prompt the user until they enter something that starts with either 'y', 'Y', 'n', or 'N'. 'y' or 'Y' is considered to be a yes answer, and will result in the function returning True. 'n' or 'N' is considered to be a no answer and will result in the function returning False. This makes it suitable for use in if/else to easily prompt users for answers.

        Parameters:
            question (str): The question to prompt the user, including a question mark. Not including '(y/n)' or any trailing spaces

        Returns:
            userResponse.lower()[0] == 'y' (str): True if the user chose yes, False if the user chose no.
    """
    userResponse = ''
    while len(userResponse) == 0 or userResponse.lower()[0] not in ('y', 'n'):
        userResponse = input(f'{question} (y/n) ')
    return userResponse.lower()[0] == 'y'


def promptMultipleChoice(question, *choices):
    """Prompt the user to choose one of the given options

    Ask the user a question, and present the available numerical options. Allow the user to enter a number to choose an option. This function handles the numbering.

    Parameters:
        question (str): The question to ask the user

        *choices (str): Any number of possible answers that the user can supply. Do not add numbers, because this function will do that automatically.

    Returns:
        choice (int): An integer from 0 to len(choices) - 1 representing the selected option: 0 being the first, 1 being the second, etc.
    """

    print(question)
    for i in range(len(choices)):
        print(f'{i + 1}. {choices[i]}')

    while True:
        try:
            choice = int(input('> ')) - 1
            if choice in range(len(choices)):
                break
        except:
            pass

        print('Invalid choice')

    return choice


def secondsToTime(seconds):
    """ Convert a timestamp in our format (seconds since midnight on Sunday) into a human-readable time of day such as "12:00 PM"

    Parameters:
        seconds (int): Number of seconds since midnight

    Returns:
        time (str): A human-readable time string
    """

    hour = (seconds % 86400) // 3600

    if hour == 0:
        hour = 12
        ap = 'AM'
    elif hour < 12:
        ap = 'AM'
    elif hour == 12:
        ap = 'PM'
    else:
        hour -= 12
        ap = 'PM'

    minute = str(((seconds % 3600) // 60)).zfill(2)

    return f'{hour}:{minute} {ap}'


def daysToStr(days, long=False):
    """ Convert a list of whether each weekday is included into a human-readable form. The human readable form can be one of the following:

    * "None" if there are no included weekdays
    * "MWF" if Monday, Wednesday, Friday, and no other days are included
    * "TTh" if Tuesday, Thursday, and no other days are included
    * "M-F" in short mode or "Weekdays" in long mode if all days from Monday to Friday are included, and the weekends are not
    * "Weekends" if the weekends are included and the weekdays are not
    * "All" in short mode or "Every day" in long mode if all days are included
    * Otherwise, a comma-separated list of days. 3 letter abbreviations in short mode, and full names in long mode

    Parameters:
        days (list): A list of 7 booleans indicating whether each particular day is included. Element 0 is Sunday.
        long (bool): Use long mode if True, or short mode if False

    Returns:
        dayString (str): A human-readable string representing which days are included
    """

    if days == [False, False, False, False, False, False, False]:
        return 'None'
    elif days == [False, True, False, True, False, True, False]:
        return 'MWF'
    elif days == [False, False, True, False, True, False, False]:
        return 'TTh'
    elif days == [False, True, True, True, True, True, False]:
        return 'Weekdays' if long else 'M-F'
    elif days == [True, False, False, False, False, False, True]:
        return 'Weekends'
    elif days == [True, True, True, True, True, True, True]:
        return 'Every day' if long else 'All'
    else:
        dayNames = ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'] if long else [
            'Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat']
        allowedDays = []
        for i in range(7):
            if days[i]:
                allowedDays.append(dayNames[i])

        return ', '.join(allowedDays)


def promptTime(prompt, instructions=False):
    """ Prompt the user to enter a time

    The time is entered as hh:mm, optionally followed by the letter a or p. If a or p is absent, it is assumed to be a 24 hour time. If a or p is present, it is assumed to be a 12 hour time, with a representing AM and p representing PM. The user entering "am" or "pm" rather than a or p is handled here, but not encouraged. Spaces in the user-provided string are also handled, but not encouraged.

    Parameters:
        prompt (str): The string to be shown to tell the user what the time is for
        instructions (bool): Whether to print instructions first. It is recommended to do this the first time the program prompts for a time.

    Returns:
        time (int): A time in our format: seconds since midnight
    """

    if instructions:
        print("Enter the time as you normally would, in hh:mm format, optionally followed by a for AM or p for PM.")
        print("If you do not indicate AM or PM, the time is assumed to be a 24 hour time")

    while True:
        userInput = input(prompt).replace(' ', '').lower().strip()

        ap = None
        if userInput[-1] == 'm':
            if userInput[-2] in ['a', 'p']:
                ap = userInput[-2]
                userInput = userInput[0:-2]
            elif not userInput[-2].isdigit():
                continue
        else:
            if userInput[-1] in ['a', 'p']:
                ap = userInput[-1]
                userInput = userInput[0:-1]
            elif not userInput[-1].isdigit():
                continue

        try:
            hour, minute = userInput.split(':')
            hour, minute = int(hour), int(minute)
        except:
            continue

        if ap == None:
            if hour not in range(24):
                continue
        else:
            if not (1 <= hour <= 12):
                continue
            if ap == 'a' and hour == 12:
                hour = 0
            elif ap == 'p' and hour != 12:
                hour += 12

        if minute not in range(60):
            continue

        return 3600 * hour + 60 * minute


def promptNumber(prompt: str, minimum: float | None = None, maximum: float | None = None) -> float:
    """ Prompt the user for a number

    Parameters:
        prompt (str): The prompt that is given to the user
        minimum (float): The minimum accepted value, inclusive. Or None if no minimum value
        maximum (float): The maximum accepted value, inclusive. Or None if no maximum value

    Returns:
        userInput (float): The number provided by the user
    """

    while True:
        try:
            userInput = float(input(prompt))
        except:
            continue

        if minimum != None and userInput < minimum:
            continue

        if maximum != None and userInput > maximum:
            continue

        return userInput


def splitByKey(items, key):
    """ For every item in a list, generate a key using the given key function, and then return a dictionary where each key is a value returned by the key function and the value is a list of all items where the key function returned that value. """

    result = {}

    for item in items:
        keyResult = key(item)
        if keyResult in result.keys():
            result[keyResult].append(item)
        else:
            result[keyResult] = [item]

    return result


def promptList(prompt):
    """ Prompt a user for a list of strings, terminated by entering a blank string

    Parameters:
        prompt (str): The string to display to tell the user what to enter

    Returns:
        items (list): The strings that have been entered by the user
    """

    items = []

    while True:
        userInput = input(prompt)
        if userInput == '':
            return items
        else:
            items.append(userInput)
