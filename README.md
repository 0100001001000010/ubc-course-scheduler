# UBC Schedule generator

Given a list of UBC classes, and some customizable rules, generate quite a lot of possible schedules

## Installation

I do not yet have a reasonable method to install this. Right now, all that is supported is cloning it. To do that, run:
```
git clone https://gitlab.com/0100001001000010/ubc-course-scheduler
cd ubc-course-scheduler
```

That will get you the latest version, which may or may not actually be usable. To see all versions that I marked as stable, run `git tag`. Then, to choose one (you probably want the latest), run `git checkout <chosen tag>`. For example, to choose version 0.2.1, run `git checkout v0.2.1`.

If you don't know how to or don't want to use the command line, locate the dropdown menu near the top left of the main area page, and select a version under "Tags". It should be directly above my profile picture. Then, click the download arrow, and select your preferred archive format. Once downloaded, extract it somewhere.

Eventually, I might make an AUR package to make installation easier on Arch-based systems, and an API and frontend to avoid having to do this.

## Usage

Run main.py. On most distros, the command should be `python main.py` assuming that your current working directory is the one you cloned the repository into. Some distros still have `python` refer to python2 for some reason. If you have one of these distros, you have to run `python3 main.py` instead.

If you already have a saved class list, you will be asked if you want to use it as is, update the data but keep the classes the same, or discard it. If you do not have a saved class list, or do not choose to use it, a new one will be generated. You will be asked which classes you want to take, Information about these classes will be scraped from the UBC website and then saved for later.

Next, you will be asked to configure the filters. This configuration will be saved for later, but you can change it when you run the program again. Then, up to 10,000 (but probably a couple hundred or thousand) schedules will be generated. You will have the option to save them to a file (recommended), or they will be printed to the terminal.

### Supported configurable filters

* Allow or disallow consideration of waitlists
* Allow or disallow consiideration of all restricted sections, or selectively allow or disallow consideration of some:
    * Allow or disallow one particular section (May be useful if all sections have different restrictions.)
    * Allow or disallow all restricted sections within a given class (May be useful if all sections have the same restrictions, such as to students of a particular major)
    * Allow or disallow all restricted sections with a given comment (useful if the section comments describe the restriction)
* Earliest time classes can start, and latest time classes can end, and maximum class time in a day
* Days of the week to allow classes on, and maximum number of days that can have classes
* Allowed or disallowed delivery modes (in person, hybrid, and online). (For example, you might want to get the in person experience, or you might be off campus.)
* Force or disallow certain sections manually. (For example, you might like or want to avoid a certain prof, might want to be in the same section as a friend, or might only want to do certain WRDS 150 topics.)
* Classes can be forced to a certain term. (For example, if you want to balance your schedule)
* Minimum break between consecutive classes
* Minimum amount of continuous time for lunch break, within a certain time range
* Minimum and maximum course count and credit hours per term


## Planned features/improvements
v0.5:
    * Add the ability to disable filters into the UI
    * Add support for requiring only one class of a set, when both classes are equivalent.
    * Per-class waitlist consideration. (For example: don't consider waitlists for required classes, but consider them for electives)
    * AUR package

v0.6
    * Algorithm performance improvements
    * Configurable performance settings:
        * Maximum queue size
        * Targeted solution size
        * Maximum execution time
        * Progress indication

v1.0:
    * API and user interface that can be self hosted or accessed from my website

## Contributing / Hacking

Contributions are welcome. The most desired contribution is adding support for more universities as it would be beneficial to the project, but I don't really have the time to support universities that I do not attend. (Plus, if the class list is only available to registered students, I may not even be able to do it if I wanted to.) However, if you have other ideas, I'll probably accept pretty much anything.

### Purposes of files (alphabetical order)
* lib.py: A couple random functions that either don't really belong in any of the other files or would be useful to multiple files.
* main.py: Interface with the user
* ratelimit.py: Network requests made through here, imposes a rate limit to avoid triggering bot detection
* schedFilter.py: Filter out schedules based on user-specified rules
* schedSolver.py: Generate schedules with the given classes
* sectionFilter.py: Filter out sections from consideration before passing them into the schedule generator
* ubc.py: Scrapes the class data from UBC

### Algorithm overview
There are classes, which have activities (such as lecture and laboratory), which have sections, which have meeting times. Registering for a section will add all of its meeting times to the schedule. It is necessary to register for at exactly one section from every activity of every class, and every section for each class (across all activities) must be for the same term. These sections are passed through a user-configurable section filter, which handles all filtering that can be handled solely by removing entire sections from consideration.

As an implementation detail, the first 2 layers are flattened into one before being sent to the main part of the schedule generation algorithm. These groups are called "class activities" because they represent a combination of a class and an activity. This makes the schedule generation algorithm simpler to implement, as now it only has to go throuh one layer of things rather than 2. These are also sorted in order of least to greatest number of sections for performance.

The algorithm's method of operation will be mostly familiar to those who have taken CPSC 110. It has 3 main lists that it works with: the schedule processing queue, the solution list, and the overflow list. The schedule processing queue represents the list of incomplete schedules that might end up as complete schedules, but which still need to be processed. These schedules are stored as a pair of sections that have been added and class activities that still need a section added. The list starts out with one schedule, with no sections added and every class activity needing a section added.

Then, the queue is looped through repeatedly until it is empty or we have found some schedules. For every item in the queue, one of 3 things can happen:

1. If the schedule is "invalid", it is removed from the queue. An invalid schedule is one that has something wrong with it, other than the fact that it is not complete yet. This could be two sections meeting at the same time, two class activities of the same class in different terms, the user-configurable schedule filter failing, etc. All schedules based on an invalid schedule would also be invalid, so removing invalid schedules from consideration immediately prevents many more from being generated and having to be filtered later.

2. If the schedule is "solved", meaning that it has no more class activities needing to be added, it is removed from the queue (because nothing more needs to be done to it) and added to the solution list.

3. If neither of the above have happened, the schedule is removed from the queue, but more schedules are generated and added to the end of the queue. The generated schedules all cover the next class activity to be added: one for each section. (In the new schedules, that class activity is no longer on the list of class activities to be added.)

Then, for performance reasons, we make a deviation from what was covered in CPSC 110. If the queue exceeds 10000 schedules, we only keep the first 10000 and dump the rest onto the "overflow" list. If the queue ends up smaller than 10000 schedules, some schedules can be taken off of the overflow list and put back onto the queue for consideration again. Yes, this is cheating because we don't technically find all possible schedules anymore. However, 10000 schedules should be enough for anyone.
