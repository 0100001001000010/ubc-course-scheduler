import json
import ubc
import schedSolver
import xdg.BaseDirectory as xdg
import os
import lib
import sectionFilter
import schedFilter

CLASS_DATA_PATH = os.path.join(
    xdg.xdg_data_home, 'classSched', 'classes.json')


def schedulesToTxt(schedules):
    """Convert a list of schedules into human readable text

    For every schedule in the list, call __str__ to produce its individual human readable representation. Leave some newlines in between them to allow the user to tell them apart easily.

        Parameters:
            schedules (list): A list of schedules

        Returns:
            schedulesTxt (str): A human-readable string representing every schedule in the list"""

    schedulesTxt = ''
    for schedule in schedules:
        schedulesTxt += 'Schedule:\n'
        lines = str(schedule).split('\n')
        for line in lines:
            schedulesTxt += f'    {line}\n'
    schedulesTxt = schedulesTxt.strip()
    return schedulesTxt


def getClassesFromUser():
    """Get a list of classes from the user and save them to disk.

    The class list is a dictionary. The key represents the name of the class, and the value is a dictionary containing the activities. This works a similar way down the whole chain. Except for the bottom where there is a plain list of meeting times, as meeting times do not have names.
    {
        "class name": {
            "activity name": {
                # For example, a lecture and lab. You must register in one section for each activity, with the restriction that all sections chosen are in the same term
                "section name": [
                    # Each section has one or more times when you are expected to be attending
                    {
                        "term": A number representing which term the class meets in
                        "restricted": Whether this section is restricted
                        "restriction": What the exact restriction for this class is
                        "credits": How many credit hours the class is worth
                        "comment": Comments attached to this section
                        "start": A number representing when the meeting time starts, in the form of seconds since midnight on Sunday
                        "end": A number representing when the meeting time ends. Sams format as above, except if the class goes past midnight from Saturday into Sunday, do not wrap around
                    },
                    ...
                ]
            }
        }
    }
    """

    classes = {}

    while True:
        userInput = input('Enter a class (or leave blank if done): ')
        if userInput == '':
            break
        else:
            try:
                classes[userInput] = ubc.scrapeClass(userInput)
            except ValueError as e:
                print(f'{userInput} is not a valid class.')

                # If a class is being marked as invalid when it really is valid, uncomment the line below to get a traceback.
                # lib.unknownError(e)
            except Exception as e:
                lib.unknownError(e)

    lib.save(CLASS_DATA_PATH, json.JSONEncoder().encode(classes))
    return classes


if os.path.exists(CLASS_DATA_PATH):
    storedClassesAction = lib.promptMultipleChoice("You have previously stored classes.",
                                                   "Keep the existing data",
                                                   "Update all section information except restrictions and credit hours (recommended)",
                                                   "Update all section information but keep the classes the same",
                                                   "Start again (necessary to change classes)")
    if storedClassesAction == 0:
        try:
            with open(CLASS_DATA_PATH) as file:
                classes = json.JSONDecoder().decode(file.read())
        except json.decoder.JSONDecodeError:
            print('File contained invalid JSON. Creating new class list.')
            classes = getClassesFromUser()
        except Exception as e:
            lib.unknownError(e)
    elif storedClassesAction == 1:
        try:
            with open(CLASS_DATA_PATH) as file:
                classes = json.JSONDecoder().decode(file.read())
                for className in classes.keys():
                    print(f'Reloading {className}')
                    classes[className] = ubc.fastUpdate(
                        className, classes[className])
                lib.save(CLASS_DATA_PATH, json.JSONEncoder().encode(classes))
        except json.decoder.JSONDecodeError:
            print('File contained invalid JSON. Creating new class list.')
            classes = getClassesFromUser()
        except Exception as e:
            lib.unknownError(e)
    elif storedClassesAction == 2:
        try:
            with open(CLASS_DATA_PATH) as file:
                classes = json.JSONDecoder().decode(file.read())
                for className in classes.keys():
                    print(f'Reloading {className}')
                    classes[className] = ubc.scrapeClass(className)
                lib.save(CLASS_DATA_PATH, json.JSONEncoder().encode(classes))
        except json.decoder.JSONDecodeError:
            print('File contained invalid JSON. Creating new class list.')
            classes = getClassesFromUser()
        except Exception as e:
            lib.unknownError(e)
    else:
        print('Creating new class list.')
        classes = getClassesFromUser()
else:
    print('Creating new class list.')
    classes = getClassesFromUser()

# Configure section filters
print(
    'Allowing waitlists' if sectionFilter.config['allowWaitlist'] else 'Not allowing waitlists')
print({
    'all': 'Allowing all restricted sections',
    'none': 'Not allowing any restricted sections',
    'custom': 'Asking whether each restricted section is allowed'
}[sectionFilter.config['allowRestricted']])
if not lib.promptYesNo('Section filters OK?'):
    sectionFilter.config['allowWaitlist'] = lib.promptYesNo(
        'Allow waitlists?')
    sectionFilter.config['allowRestricted'] = ['all', 'none', 'custom'][lib.promptMultipleChoice(
        'Allow restricted sections?',
        'All restricted sections',
        'No restricted sections',
        'Ask every time'
    )]
    lib.save(sectionFilter.CONFIG_PATH,
             json.JSONEncoder().encode(sectionFilter.config))

undefinedFilters = sectionFilter.getUndefinedFilters(classes)

# Doing this rather than iterating because filters based on comments or class name will make us have to regenerate the list
while len(undefinedFilters) > 0:
    question = f'Do you meet the restrictions for {undefinedFilters[0]["section"]}?'
    choices = [
        'Yes, for this section only',
        'No, for this section only',
        'Yes, for all sections in this class',
        'No, for all sections in this class'
    ]

    if undefinedFilters[0]['comment'] != '':
        question += f'\nComment: {undefinedFilters[0]["comment"]}'
        choices += [
            'Yes, for all sections with this comment',
            'No, for all sections with this comment'
        ]

    question += f'\nSection info: {ubc.getLink(undefinedFilters[0]["section"])}'

    response = lib.promptMultipleChoice(question, *choices)

    if response == 0:
        sectionFilter.config['allowedSections'].append(
            undefinedFilters[0]['section'])
        del undefinedFilters[0]
    elif response == 1:
        sectionFilter.config['disallowedSections'].append(
            undefinedFilters[0]['section'])
        del undefinedFilters[0]
    elif response == 2:
        sectionFilter.config['allowedClasses'].append(
            undefinedFilters[0]['class'])
        undefinedFilters = sectionFilter.getUndefinedFilters(classes)
    elif response == 3:
        sectionFilter.config['disallowedClasses'].append(
            undefinedFilters[0]['class'])
        undefinedFilters = sectionFilter.getUndefinedFilters(classes)
    elif response == 4:
        sectionFilter.config['allowedComments'].append(
            undefinedFilters[0]['comment'])
        undefinedFilters = sectionFilter.getUndefinedFilters(classes)
    else:
        sectionFilter.config['disallowedComments'].append(
            undefinedFilters[0]['comment'])
        undefinedFilters = sectionFilter.getUndefinedFilters(classes)

if type(sectionFilter.config['dayStart']) is int:
    print(
        f'Class day starts at {lib.secondsToTime(sectionFilter.config["dayStart"])}')
else:
    print('No defined class day start time. Classes can start as early as necessary.')
if type(sectionFilter.config['dayEnd']) is int:
    print(
        f'Class day ends at {lib.secondsToTime(sectionFilter.config["dayEnd"])}')
else:
    print('No defined class day end time. Classes can end as late as necessary.')
print(f'Allowed days: {lib.daysToStr(sectionFilter.config["allowedDays"])}')

if not lib.promptYesNo('Allowed days/times OK?'):
    dayStart = lib.promptTime('Enter start time: ', instructions=True)
    dayEnd = lib.promptTime('Enter end time: ')
    if dayEnd <= dayStart:
        dayEnd += 86400
    sectionFilter.config['dayStart'] = dayStart
    sectionFilter.config['dayEnd'] = dayEnd

    sectionFilter.config['allowedDays'] = [lib.promptYesNo(
        f'Allow classes on {["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"][i]}?') for i in range(7)]

# Allowed modes of delivery
print('Allowed modes of delivery: ', end='')
print(', '.join(filter(
    lambda mode: sectionFilter.config['modes'][mode], sectionFilter.config['modes'].keys())))
if not lib.promptYesNo('Modes of delivery OK?'):
    for mode in sectionFilter.config['modes'].keys():
        sectionFilter.config['modes'][mode] = lib.promptYesNo(
            f'Allow {mode} classes?')

# Preferred terms
if len(sectionFilter.config['term1']) == 0:
    print('No classes forced to term 1')
else:
    print(
        f'Classes forced to term 1: {", ".join(sectionFilter.config["term1"])}')

if len(sectionFilter.config['term2']) == 0:
    print('No classes forced to term 2')
else:
    print(
        f'Classes forced to term 2: {", ".join(sectionFilter.config["term2"])}')

if not lib.promptYesNo('Terms OK?'):
    sectionFilter.config['term1'] = lib.promptList(
        'Enter a class that you want to force to term 1. Leave blank when done: ')
    sectionFilter.config['term2'] = lib.promptList(
        'Enter a class that you want to force to term 2. Leave blank when done: ')

# Preferred and unwanted sections
if len(sectionFilter.config['preferredSections']) == 0:
    print('No preferred sections')
else:
    for className in sectionFilter.config['preferredSections'].keys():
        for activityName in sectionFilter.config['preferredSections'][className].keys():
            print(
                f'Preferred sections in {className} {activityName}: {", ".join(sectionFilter.config["preferredSections"][className][activityName])}')

if len(sectionFilter.config['unwantedSections']) == 0:
    print('No unwanted sections')
else:
    for className in sectionFilter.config['unwantedSections'].keys():
        for activityName in sectionFilter.config['unwantedSections'][className].keys():
            print(
                f'Unwanted sections in {className} {activityName}: {", ".join(sectionFilter.config["unwantedSections"][className][activityName])}')

if not lib.promptYesNo('Preferred/unwanted sections OK?'):
    sectionFilter.config['preferredSections'] = {}
    sectionFilter.config['unwantedSections'] = {}
    # Applying the filter first prevents the user from being asked about classes that are already filtered out
    classes = sectionFilter.applyFilter(classes)

    for className in classes.keys():
        classPreferred = {}
        classUnwanted = {}
        for activityName in classes[className].keys():
            sections = classes[className][activityName]

            print('If preferred sections are provided for a class activity, only those sections will be allowed to be used. If unwanted sections are provided, all sections are allowed except for the ones specified')

            print(
                f'{className} {activityName} sections are {", ".join(sections.keys())}')
            toSpecify = lib.promptMultipleChoice(
                'What should be specified?',
                'Preferred sections (only allow these)',
                'Unwanted sections (allow all except these)',
                'Neither (allow all sections)'
            )

            preferredSections = []
            unwantedSections = []

            if toSpecify == 0:
                preferredSections = lib.promptList(
                    'Enter a preferred section, or leave blank if done: ')
            elif toSpecify == 1:
                unwantedSections = lib.promptList(
                    'Enter an unwanted section, or leave blank if done: ')

            if len(preferredSections) > 0:
                classPreferred[activityName] = preferredSections
            if len(unwantedSections) > 0:
                classUnwanted[activityName] = unwantedSections

        if len(classPreferred.keys()) > 0:
            sectionFilter.config['preferredSections'][className] = classPreferred
        if len(classUnwanted.keys()) > 0:
            sectionFilter.config['unwantedSections'][className] = classUnwanted


lib.save(sectionFilter.CONFIG_PATH,
         json.JSONEncoder().encode(sectionFilter.config))
classes = sectionFilter.applyFilter(classes)
print('Allowed sections:')
for className in classes.keys():
    print(f'\t{className}: ', end='')
    for activityName in classes[className].keys():
        for sectionName in classes[className][activityName].keys():
            print(sectionName, end=' ')
    print()

# Configure schedule filters
print(
    f'Minimum time between classes: {schedFilter.config["minBreak"] / 60} minutes')
if schedFilter.config['lunchMinTime'] == 0:
    print('No lunch break required')
else:
    print(
        f'Minimum {schedFilter.config["lunchMinTime"] / 60} minute lunch break between {lib.secondsToTime(schedFilter.config["lunchStart"])} and {lib.secondsToTime(schedFilter.config["lunchEnd"])}')
print(
    f'Up to {schedFilter.config["maxTime"] / 3600} hours of class per day, {int(schedFilter.config["maxDays"])} days per week')
if schedFilter.config['minCourses'] == -1:
    print('No minimum course count per term')
else:
    print(f'Minimum {schedFilter.config["minCourses"]} courses per term')
if schedFilter.config['maxCourses'] == -1:
    print('No maximum course count per term')
else:
    print(f'Maximum {schedFilter.config["maxCourses"]} courses per term')
if schedFilter.config['minCredits'] == -1:
    print('No minimum credit count per term')
else:
    print(f'Minimum {schedFilter.config["minCredits"]} credits per term')
if schedFilter.config['maxCredits'] == -1:
    print('No maximum credit count per term')
else:
    print(f'Maximum {schedFilter.config["maxCredits"]} credits per term')

if not lib.promptYesNo("Is this OK?"):
    schedFilter.config["minBreak"] = int(lib.promptNumber(
        'Minimum number of minutes between classes: ', 0, None) * 60)
    schedFilter.config["lunchMinTime"] = int(lib.promptNumber(
        'Minimum number of minutes for lunch: ', 0, None) * 60)
    if schedFilter.config["lunchMinTime"] != 0:
        schedFilter.config['lunchStart'] = lib.promptTime(
            'Lunch break earliest start time: ', instructions=True)
        schedFilter.config['lunchEnd'] = lib.promptTime(
            'Lunch break latest end time: ', instructions=True)
    schedFilter.config['maxTime'] = int(lib.promptNumber(
        'Maximum class hours per day: ', 0, None) * 3600)
    schedFilter.config['maxDays'] = int(lib.promptNumber(
        'Maximum class days per week: ', 1, 7))
    schedFilter.config['minCourses'] = int(lib.promptNumber(
        'Minimum courses per term (-1 for no limit): ', -1))
    schedFilter.config['maxCourses'] = int(lib.promptNumber(
        'Maximum courses per term (-1 for no limit): ', -1))
    schedFilter.config['minCredits'] = int(lib.promptNumber(
        'Minimum credit hours per term (-1 for no limit): ', -1))
    schedFilter.config['maxCredits'] = int(lib.promptNumber(
        'Maximum credit hours per term (-1 for no limit): ', -1))

    lib.save(schedFilter.CONFIG_PATH,
             json.JSONEncoder().encode(schedFilter.config))

print('Generating schedules. This might take a while, depending on the speed of your computer.')
schedules = schedSolver.solve(classes, schedFilter)

if len(schedules) == 0:
    print('No schedules were found. Suggested solutions:')
    print('    * Check to see if any classes have all sections full')
    print('    * Change your filters to be less restrictive')
    print('    * If you know that there should be schedules, there may be a bug in the program. You may report it at https://gitlab.com/bruceblore/ubc-course-scheduler/-/issues. Make sure to include the contents of the following files in your report:')
    print(f'        * {CLASS_DATA_PATH}')
    print(f'        * {sectionFilter.CONFIG_PATH}')
    print(f'        * {schedFilter.CONFIG_PATH}')
else:
    schedulesTxt = schedulesToTxt(schedules)
    print(f'Done! Generated {len(schedules)} schedules.')
    if lib.promptYesNo('Save schedules to file?'):
        lib.save(input('File name: '), schedulesTxt)
    else:
        print(schedulesTxt, end='')
