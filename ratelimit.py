import urllib.request
from time import sleep
import random
from datetime import datetime, timedelta


class RateLimit:
    avgDelay: float
    random: float
    nextRequest: datetime

    def __init__(self, avgDelay: float, random: float):
        """Initialize a RateLimit object

        Parameters:
            avgDelay (float): The average delay we want between requests
            random (float): The amount of time that the delay should be allowed to vary by

        Returns:
            self (RateLimit): The RateLimit object
        """

        self.avgDelay = avgDelay
        self.random = random
        self.nextRequest = datetime.now()

    def request(self, url: str, headers: dict) -> bytes:
        """Perform a request, waiting if necessary to avoid exceeding the ratelimit

        Parameters:
            url (str): The URL to request
            headers (dict): The HTTP headers to send with the request

        Returns:
            html (str): The HTML of the page as a string
        """

        sleepTime: float = (self.nextRequest - datetime.now()).total_seconds()
        if sleepTime > 0:
            sleep(sleepTime)

        page: urllib.request._UrlopenRet = urllib.request.urlopen(
            urllib.request.Request(url, data=None, headers=headers))
        html = page.read()

        delay = self.avgDelay + ((random.random() - 0.5) * 2 * self.random)
        self.nextRequest = datetime.now() + timedelta(seconds=delay)

        return html
