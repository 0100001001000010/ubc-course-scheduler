import logo from './logo.svg';
import './App.css';
import * as React from 'react';

function App() {
  const useStates = [React.useState(false), React.useState(false), React.useState(false), React.useState(false), React.useState(false), React.useState(false), React.useState(false),]
  const DAYS = [
    {
      timestamp: 0,
      name: 'Sunday',
      checked: useStates[0][0],
      setChecked: useStates[0][1]
    },
    {
      timestamp: 86400,
      name: 'Monday',
      checked: useStates[1][0],
      setChecked: useStates[1][1]
    },
    {
      timestamp: 172800,
      name: 'Tuesday',
      checked: useStates[2][0],
      setChecked: useStates[2][1]
    },
    {
      timestamp: 259200,
      name: 'Wednesday',
      checked: useStates[3][0],
      setChecked: useStates[3][1]
    },
    {
      timestamp: 345600,
      name: 'Thursday',
      checked: useStates[4][0],
      setChecked: useStates[4][1]
    },
    {
      timestamp: 432000,
      name: 'Friday',
      checked: useStates[5][0],
      setChecked: useStates[5][1]
    },
    {
      timestamp: 518400,
      name: 'Saturday',
      checked: useStates[6][0],
      setChecked: useStates[6][1]
    }
  ]

  let [hstart, sethstart] = React.useState(9)
  let [mstart, setmstart] = React.useState(0)
  let [hend, sethend] = React.useState(10)
  let [mend, setmend] = React.useState(0)

  return (
    <div className="App">
      <div>
        {DAYS.map(day => <label>
          <input type="checkbox" checked={day.checked} onChange={() => { day.setChecked(!day.checked) }}></input>
          {day.name}
        </label>)}
      </div>
      <div>
        <input type="number" value={hstart} onChange={event => sethstart(event.target.value)}></input>:
        <input type="number" value={mstart} onChange={event => setmstart(event.target.value)}></input>-
        <input type="number" value={hend} onChange={event => sethend(event.target.value)}></input>:
        <input type="number" value={mend} onChange={event => setmend(event.target.value)}></input>
      </div>
      {DAYS.filter(day => day.checked).map(day => <div>{day.name}: {day.timestamp + hstart * 3600 + mstart * 60}-{day.timestamp + hend * 3600 + mend * 60}</div>)}
    </div>
  );
}

export default App;
