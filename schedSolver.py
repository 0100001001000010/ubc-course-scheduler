import itertools
import lib


class Schedule:
    """Represents a schedule as it is being solved, plus some extra data to make it easier

    Attributes
    ----------
    sections : dict
        All sections that are in this schedule so far, and all that is retained of the Schedule after it is completed
    classesRemaining : dict
        All classes that do not yet have a section added

    Methods
    -------
    nextSchedules():
        Returns a list of schedules where each schedule has a different section from the next class activity to add
    invalid():
        Returns whether there is something wrong with the current schedule
    solved():
        Checks whether the schedule is ready to be added to the solution list
    __str__():
        Converts the schedule into human readable form
    """

    def __init__(self, sections, classes):
        self.sections = sections
        self.classesRemaining = classes

    def nextSchedules(self):
        """Generate all next schedules. Essentially, make a bunch of copies of the schedule with every possible "next step" being done. Over time, this builds up all possible schedules.

        If there are no class activities remaining, there are no next schedules.

        If the first class activity has no sections, remove it from consideration.

        Otherwise, each next schedules have the same sections as the current one, as well as a section from the first class activity to add, with each one having a different section. Each next schedule will no longer have that activity on its list of classes remaining to be added, as it must not be added again to avoid duplicates.

        Returns:
            schedules (list): Every schedule with the next class activity added
        """

        if len(self.classesRemaining) == 0:
            return []

        else:
            return [Schedule(
                {**self.sections,
                    sectionName: self.classesRemaining[0][sectionName]},
                self.classesRemaining[1:]
            ) for sectionName in self.classesRemaining[0].keys()]

    def invalid(self, schedFilter):
        """Return whether the schedule is invalid, meaning that one of the following is true:

        * There is at least one point in time where there is more than one class to attend.

        * A section is registered in activities for the same class but from different terms. (For example, a lecture in term 1 and a lab in term 2)

        This does not check whether there is more than one section for the same class activity, because nextSchedules cannot assign more than one section for the same class activity.

        An invalid schedule should not have the next schedules generated or stored, because they will also be invalid.
        """

        sectionNames = list(self.sections.keys())

        # If there are no sections added, just assume that the schedule is valid because there is nothing to cause problems and our checks won't work in this case
        if len(sectionNames) == 0:
            return False

        # All pre-existing sections are known to not have any problems with each other, so we only have to check the most recently added one to see if it introduces any problems. This reduces the problem from O(n^2) to O(n)
        lastSectionName = sectionNames[-1]
        exceptLastSectionName = sectionNames[:-1]

        # Check for multiple classes meeting at the same time
        def timeOverlap(a, b):
            if a['term'] != b['term']:
                return False
            elif a['start'] < a['end'] <= b['start'] < b['end']:
                return False
            elif b['start'] < b['end'] <= a['start'] < a['end']:
                return False
            else:
                return True

        for compareSectionName in exceptLastSectionName:
            timeSlotCombinations = itertools.product(
                self.sections[compareSectionName], self.sections[lastSectionName])
            for combination in timeSlotCombinations:
                if timeOverlap(*combination):
                    return True

        # Check for any other activities in different terms
        def getTerm(section):
            term1 = False
            term2 = False

            for meetingTime in section:
                if meetingTime['term'] == 1:
                    term1 = True
                elif meetingTime['term'] == 2:
                    term2 = True
                else:
                    raise ValueError('Invalid term in class list')

            if term1 and not term2:
                return 1
            elif term2 and not term1:
                return 2
            elif term1 and term2:
                return 'both'
            else:
                return None

        if not schedFilter.preFilter(self):
            return True

        for compareSectionName in exceptLastSectionName:
            # All meeting times from the same section will have the same class, so we can just grab the first ones and they'll be valid for the whole section
            # Because we only add one section for every class/activity combination, if the classes are equal we can assume that the activities are not equal and therefore do not have to check them.
            compareSectionClass = self.sections[compareSectionName][0]['class']
            lastSectionClass = self.sections[lastSectionName][0]['class']

            # If these sections are not from the same class, we don't care about them
            if compareSectionClass != lastSectionClass:
                continue

            # If it's a 2-term section, not all meeting times will have the same term, so use a helper function to actually search for this. (We don't need the helper, but it makes the code more readable)
            compareSectionTerm = getTerm(self.sections[compareSectionName])
            lastSectionTerm = getTerm(self.sections[lastSectionName])

            # At this point, we can assume that the 2 sections are from the same class, but are different activities
            if compareSectionTerm != lastSectionTerm:
                return True

        # If both checks have not caused the section to be marked invalid, it is valid
        return False

    def solved(self):
        """Return whether the schedule is solved, meaning that there are no class activities that do not have a section chosen.

        This does not check whether the schedule is valid or not, because it is assumed that check will already have been done.

        A solved schedule should not have the next schedules generated because there will not be any.
        """

        return len(self.classesRemaining) == 0

    def __str__(self):
        """Convert to a string, which consists of a formatted list of all sections in the schedule, intended to be human readable.

        The format is a list of all sections in the schedule, each with a sublist of meeting times in the form of Term {term} {day} {time range}.

        Do not indent an extra level. That is up to the caller
        """

        result = ''

        def timeString(meetingTime):
            """ Get the day time range of the given meeting time """

            # day = ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday',
            #    'Friday', 'Saturday'][meetingTime['start'] // 86400]

            startSec = meetingTime['start']
            endSec = meetingTime['end']

            start = f'{startSec % 86400 // 3600}:{str(startSec % 3600 // 60).zfill(2)}'
            end = f'{endSec % 86400 // 3600}:{str(endSec % 3600 // 60).zfill(2)}'

            return f'{start}-{end}'

        for sectionName in self.sections.keys():
            # Sort meeting times in order of time because it's most convenient to do it here, and split by term
            meetingTimes = self.sections[sectionName]
            meetingTimes.sort(key=lambda meetingTime: meetingTime['start'])
            meetingTimes = lib.splitByKey(
                meetingTimes, lambda meetingTime: meetingTime['term'])

            # Using [1, 2] rather than meetingTimes.keys to force term 1 to be before term 2
            for term in [1, 2]:
                if term not in meetingTimes.keys():
                    continue

                meetTimesInTerm = lib.splitByKey(
                    meetingTimes[term], timeString)
                timeRangeStrings = []
                for timeRange in meetTimesInTerm.keys():
                    meetingDays = [False, False, False,
                                   False, False, False, False]
                    for meetingTime in meetTimesInTerm[timeRange]:
                        meetingDays[meetingTime['start'] // 86400] = True
                    daysStr = lib.daysToStr(meetingDays)
                    timeRangeStrings.append(f'{daysStr} {timeRange}')

                result += f'{sectionName} (Term {term}): {"; ".join(timeRangeStrings)}\n'

        return result


def solve(classes, schedFilter):
    """Find many possible schedules

    There shall be no schedules with conflicts, and all schedules should have a section from every activity of every class

        Parameters:
            classes (list): Every class that needs to be taken, as described in main.py

        Returns:
            schedulesFound (list): A list of every possible schedule, where each schedule is a dictionary of sections. The key represents the user-friendly name for the section, and the value is a list of dictionaries representing meeting times with the following info:
            {
                # term, start, and end are the same as the class list
                "term": A number representing which term the class meets in
                "start": A number representing when the meeting time starts, in the form of seconds since midnight on Sunday
                "end": A number representing when the meeting time ends. Sams format as above, except if the class goes past midnight from Saturday into Sunday, do not wrap around

                # Additional information to replace what would otherwise be lost while flattening the dictionary to one level
                "class": The class that the meeting time belongs to
                "activity": Which activity the meeting time belongs to
            }
    """

    def preprocess(classes):
        """Flatten a list of classes and sort the class activities in order from least to most sections

        Parameters:
            classes (list): Every class that needs to be taken, as described in main.py

        Returns:
            activities (list): A list of class activities, where every activity is a dictionary of sections. The key represents the user-friendly name for the section, and the value is a list of dictionaries representing meeting times with the following info:
            {
                # term, start, and end are the same as the class list
                "term": A number representing which term the class meets in
                "start": A number representing when the meeting time starts, in the form of seconds since midnight on Sunday
                "end": A number representing when the meeting time ends. Sams format as above, except if the class goes past midnight from Saturday into Sunday, do not wrap around

                # Additional information to replace what would otherwise be lost while flattening the dictionary to one level
                "class": The class that the meeting time belongs to
                "activity": Which activity the meeting time belongs to
            }
        """

        activities = []

        for className in classes.keys():
            for activityName in classes[className].keys():
                activity = classes[className][activityName]

                # Add the class and activity data to the section meet times
                for sectionName in activity.keys():
                    section = activity[sectionName]
                    for meetingTime in section:
                        meetingTime['class'] = className
                        meetingTime['activity'] = activityName

                # Add the activity to the list of activities, performing the flattening
                activities.append(activity)

        # Sort by the number of sections in each class activity
        activities.sort(key=lambda activity: len(
            activity.keys()))

        return activities

    schedulesToCheck = [Schedule({}, preprocess(classes))]
    schedulesFound = []
    overflow = []
    MAX_LIST_SIZE = 10000

    # Stop either when we run out of schedules to check or we find some solutions
    while len(schedulesToCheck) > 0 and len(schedulesFound) == 0:
        nextSchedulesToCheck = []
        for schedule in schedulesToCheck:
            if schedule.invalid(schedFilter):
                continue
            elif schedule.solved():
                if schedFilter.postFilter(schedule):
                    schedulesFound.append(schedule)
            else:
                nextSchedulesToCheck += schedule.nextSchedules()

        # Adds excess schedules to overflow cache
        overflow += nextSchedulesToCheck[MAX_LIST_SIZE:]
        # Preserves only a limited number of schedules, and if there are too few pull some off of the overflow cache
        schedulesToCheck = (nextSchedulesToCheck + overflow)[0:MAX_LIST_SIZE]
        overflow = overflow[max(MAX_LIST_SIZE - len(nextSchedulesToCheck), 0):]

    return schedulesFound
