import copy
import lib
import json
import os
import xdg.BaseDirectory as xdg
from schedSolver import Schedule

CONFIG_PATH = os.path.join(xdg.xdg_config_home,
                           'classSched', 'schedFilters.json')

DEFAULT_CONFIG = {
    # Minimum time between adjacent classes, in seconds.
    # NOTE: UBC classes typically end 10 minutes before the time indicated on the SSC. This program uses the time indicated on the SSC for all calculations. This means that if you leave this at zero, effectively "disabling" the feature by allowing classes to have no break according to the schedule times, you will still have 10 minutes to get between your classes.
    'minBreak': 0,

    # lunchMinTime is the minimum amount of time (in seconds) to be left free for lunch, between lunchStart and lunchEnd which are represented as seconds since midnight.
    'lunchMinTime': 1800,   # 30 minutes
    'lunchStart': 39600,    # 11 AM
    'lunchEnd': 46800,      # 1 PM

    # Maximum amount of class time per day
    'maxTime': 14400,    # 6 hours

    # Maximum number of days with class
    'maxDays': 5,

    # Minimum and maximum number of courses and credit hours per term, or -1 to enforce no limit
    'minCourses': -1,
    'maxCourses': 6,
    'minCredits': 9,
    'maxCredits': -1
}

if os.path.exists(CONFIG_PATH):
    try:
        with open(CONFIG_PATH) as file:
            config = {**DEFAULT_CONFIG, **
                      json.JSONDecoder().decode(file.read())}
    except json.JSONDecodeError:
        config = DEFAULT_CONFIG
else:
    config = DEFAULT_CONFIG


def preFilter(schedule: Schedule):
    """ Decides whether a given schedule should be included based on the settings

    Assumes that the provided schedule is otherwise valid

    Don't check whether there are too few courses/credits, because all new schedules will have too few

    Parameters:
        schedule (Schedule): A SchedSolver Schedule object

    Returns:
        shouldInclude (bool): True if the schedule should be included according to the filters, False if the schedule should be excluded
    """

    meetingTimes = []
    for sectionName in schedule.sections.keys():
        meetingTimes += schedule.sections[sectionName]

    def dayModulus(meetingTime):
        """ Modulo the start and end time by the number of seconds in a day to allow for time of day comparisons to be made """

        result = copy.copy(meetingTime)
        result['start'] = meetingTime['start'] % 86400
        result['end'] = meetingTime['end'] % 86400

        if result['end'] <= result['start']:
            result['end'] += 86400

        return result

    meetingTimes = lib.splitByKey(
        meetingTimes, lambda meetingTime: meetingTime['term'])
    for term in meetingTimes.keys():
        meetTimesInTerm = meetingTimes[term]
        meetTimesInTerm.sort(key=lambda meetingTime: meetingTime['start'])

        # Check whether there are any minimum break violations
        # Doing it this way because we need to access the index number to compare with the next item
        for i in range(len(meetTimesInTerm)):
            # If we're not at the end, compare with the next item
            if i < len(meetTimesInTerm) - 1:
                # Check whether the difference between this meeting time's end and the next one's start is less than the configued minimum break. If so, return immediately.
                if meetTimesInTerm[i + 1]['start'] - meetTimesInTerm[i]['end'] < config['minBreak']:
                    return False
            # If we are at the end, compare with the first item, adding a full week to the start time to make it relative to the same refernce point
            else:
                if (meetTimesInTerm[0]['start'] + 604800) - meetTimesInTerm[i]['end'] < config['minBreak']:
                    return False

        # Split by day of week
        meetTimesInTerm = lib.splitByKey(
            meetTimesInTerm, lambda meetingTime: meetingTime['start'] // 86400)

        # Check maximum class days per week
        if len(meetTimesInTerm.keys()) > config['maxDays']:
            return False

        for day in meetTimesInTerm.keys():
            meetTimesInDay = list(map(dayModulus, meetTimesInTerm[day]))

            # Check maximum meeting time per day
            if sum(map(lambda meetingTime: meetingTime['end'] - meetingTime['start'], meetTimesInDay)) > config['maxTime']:
                return False

            # Check minimum lunch break
            breaks = []
            # First, find all of today's breaks, in the form of tuples with the start and end times
            for i in range(len(meetTimesInDay)):
                # If this is the first meeting time of the day, there is a break from the start of the day to the start of the meeting time
                if i == 0:
                    breaks.append((0, meetTimesInDay[i]['start']))
                # If this is the last meeting time of the day, there is a break from the end of the meeting time to the end of the day
                # NOTE: Using a new if rather than an elif because a meeting time being the first is not mutually exclusive with it being the last or having more after it
                if i == len(meetTimesInDay) - 1:
                    breaks.append((meetTimesInDay[i]['end'], 86400))
                # If this is not the last meeting time of the day, there is a break from the end of this meeting time to the start of the next one
                else:
                    breaks.append(
                        (meetTimesInDay[i]['start'], meetTimesInDay[i + 1]['end']))

            # Clamp all times to be within the lunch break range
            breaks = list(
                map(
                    lambda _break: (
                        min(max(_break[0], config['lunchStart']),
                            config['lunchEnd']),
                        min(max(_break[1], config['lunchStart']),
                            config['lunchEnd'])
                    ),
                    filter(
                        lambda _break: 0 <= _break[0] <= _break[1] <= 86400,
                        breaks
                    )
                )
            )

            # Find the longest break length
            longestBreak = max(
                *map(lambda _break: _break[1] - _break[0], breaks))

            if longestBreak < config['lunchMinTime']:
                return False

    # Check for maximum courses and credits per term
    # Do not check for minimum because all schedules will fail immediately, before the minimum can be reached
    term1Courses = 0
    term2Courses = 0
    term1Credits = 0
    term2Credits = 0
    classesCounted = set()

    for sectionName in schedule.sections.keys():
        _class = schedule.sections[sectionName][0]['class']
        if _class in classesCounted:
            continue
        classesCounted.add(_class)
        term = schedule.sections[sectionName][0]['term']
        credits = schedule.sections[sectionName][0]['credits']

        if term == 1:
            term1Courses += 1
            term1Credits += credits
        elif term == 2:
            term2Courses += 1
            term2Credits += credits
        else:
            term1Courses += 1
            term2Courses += 1
            term1Credits += credits
            term2Credits += credits

    if config['maxCourses'] != -1:
        if term1Courses > config['maxCourses'] or term2Courses > config['maxCourses']:
            return False
    if config['maxCredits'] != -1:
        if term1Credits > config['maxCredits'] or term2Credits > config['maxCredits']:
            return False

    return True


def postFilter(schedule: Schedule):
    """Run validity checks that were skipped in preFilter

    Right now, this is the minimum class and credit hour check

    Assume that the schedule is valid and solved

    Parameters:
        schedule (Schedule): A SchedSolver Schedule object

    Returns:
        shouldInclude (bool): True if the schedule should be included according to the filters, False if the schedule should be excluded
    """
    term1Courses = 0
    term2Courses = 0
    term1Credits = 0
    term2Credits = 0
    classesCounted = set()

    for sectionName in schedule.sections.keys():
        _class = schedule.sections[sectionName][0]['class']
        if _class in classesCounted:
            continue
        classesCounted.add(_class)
        term = schedule.sections[sectionName][0]['term']
        credits = schedule.sections[sectionName][0]['credits']
        if term == 1:
            term1Courses += 1
            term1Credits += credits
        elif term == 2:
            term2Courses += 1
            term2Credits += credits
        else:
            term1Courses += 1
            term2Courses += 1
            term1Credits += credits
            term2Credits += credits

    if config['minCourses'] != -1:
        if term1Courses < config['minCourses'] or term2Courses < config['minCourses']:
            return False
    if config['minCredits'] != -1:
        if term1Credits < config['minCredits'] or term2Credits < config['minCredits']:
            return False

    return True
