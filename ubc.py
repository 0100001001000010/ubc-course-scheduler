from ratelimit import RateLimit
from bs4 import BeautifulSoup
import itertools
from enum import Enum, auto
import copy
import lib

# A string which, if present, indicates that the course is not actually offered. This is used because at the time this code was written, UBC returned HTTP 200, not 404, when the course was not found. (Same as when it was.)
COURSE_NOT_FOUND = "The requested course is either no longer offered at UBC Vancouver or is not being offered this session"

# These strings, if they appear in a status feild, indicate a section that you may not register for.
# Blocked, Unreleased - Staff have prohibited registration for some reason, but might allow it in the future
# Cancelled - The section was offered, but is not offered anymore
# Full - All seats have been taken
# STT - You cannot register for this section individually, only as part of a standard time table
# Note: "Restricted" is intentionally NOT included here. It means that only restricted seats remain, and the user may or may not meet the restrictions. They are to be filtered out later.
DISALLOWED_STATUSES = ['Blocked', 'Unreleased', 'Cancelled', 'Full', 'STT']

# If any of these values appear in the table, treat it as though the course is a full year course.
FULL_YEAR_TERM_VALUES = [None, '', '1-2']

# If you just send a request using the default urllib headers, wget, curl, etc, it triggers bot detection and requires a CAPTCHA. Send these headers to pretend to be a browser.
# Note: I just copied all the headers my browser sent. I do not know which exact ones are needed, but I do know that just user agent is not enough.
REQUEST_HEADERS = {
    'Accept': 'text/html, application/xhtml+xml, application/xml',
    'Accept-Encoding': 'gzip, deflate, br',
    'Accept-Language': 'en-US, en',
    'Cache-Control': 'no-cache',
    'Connection': 'keep-alive',
    'DNT': '1',
    'Host': 'courses.students.ubc.ca',
    'Pragma': 'no-cache',
    'Sec-Fetch-Dest': 'document',
    'Sec-Fetch-Mode': 'navigate',
    'Sec-Fetch-Site': 'same-origin',
    'Sec-Fetch-User': '?1',
    'Sec-GPC': '1',
    'Upgrade-Insecure-Requests': '1',
    'User-Agent': 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36',
    'sec-ch-ua': '"Not.A/Brand"',
    'sec-ch-ua-mobile': '?0',
    'sec-ch-ua-platform': '"Linux"'
}

rateLimit = RateLimit(1.5, 0.75)


def _meetTimeToDict(meetTime):
    """Convert a meeting time into a dictionary for easier processing later.

    This is still a representation of exactly what was on the page. It is not yet converted into the format for use by the rest of the program.

    Parameters:
        section (bs4.element.Tag): A table row representing a section, in the form of a Beautifulsoup object

    Returns:
        section (dict): A dictionary representing the section, with the following elements:
            * 'htmlSection': The HTML class of the table row, representing which meeting times go together
            * 'status': Whether the class is full, blocked, restricted, etc. Full and blocked sections should be excluded (later, not here) because nobody can register for them. Restricted sections should still be included because it is possible that the user might meet the restrictions.
            * 'section': An identifier for the section.
            * 'activity': Which activity the course is. (Later, these will be split into separate classes.)
            * 'term': Which term(s) a class takes place in
            * 'modeOfDelivery': How the class is delivered
            * 'interval': I don't know what this means, but the website has it and it always seems to be blank.
            * 'startTime': Represents the time the class starts, 24 hour format. Kept as it is represented on the website, not converted into our format.
            * 'endTime': Represents the time the class ends, 24 hour format. Kept as it is represented on the website, not converted into our format.
            * 'comments': Comments for that section
            * 'inPersonRequired': Whether in person attendance is required
    """

    return dict(zip(['htmlSection', 'status', 'section', 'activity', 'term', 'modeOfDelivery', 'interval', 'days', 'startTime', 'endTime', 'comments', 'inPersonRequired'], [meetTime.get_attribute_list('class')[0]] + meetTime.find_all('td')))


def _dictsToClass(meetingTimes, className):
    """Convert a list of dictionaries from _meetTimeToDict into our own format, representing a class.

    It is necessary to operate on the whole list rather than just each individual section because some sections are spread out over multiple rows of the table

    Paramteres:
        meetingTimes (list): A list of dictionaries from _meetTimeToDict to be converted into

        className (str): The user-friendly name for the class

    Returns:
        result (list): A class in our format
    """

    def group(nameFunction, meetingTimes, **kwargs):
        """ Group the given meeting times according to the value generated by the given function. Or, if the function returns None, do not include the meeting time.

        Parameters:
            nameFunction (function): A function which returns the name of the group that the meeting time belongs to. Alternatively, it can also return None if the meeting time should not be considered.

            meetingTimes (list): A list of meeting times from _meetingTimeToDict to categorize

            mapFunction (function): An optional function that if defined, can be used to map the meeting times to other values

        Returns:
            A dictionary mapping group names to lists of things in that group
        """
        groups = {}

        for meetingTime in meetingTimes:
            groupName = nameFunction(meetingTime)
            if 'mapFunction' in kwargs.keys():
                mapped = kwargs['mapFunction'](meetingTime)
            else:
                mapped = meetingTime

            if groupName == None:
                continue
            elif groupName in groups.keys():
                groups[groupName].append(mapped)
            else:
                groups[groupName] = [mapped]

        return groups

    # Group the meeting times by activity. Also filter out sections that do not have a valid meeting time, as we don't have a better place to do that.
    def getActivityName(meetingTime):
        activity = meetingTime['activity'].getText()
        if meetingTime['days'].getText().strip() == '' or meetingTime['startTime'].getText().strip() == '' or meetingTime['endTime'].getText().strip() == '':
            return None
        elif className == 'SCIE 113' and activity == 'Lecture' and 'GP' in meetingTime['section'].getText():
            return 'Speaker Series'
        else:
            return activity

    result = group(getActivityName, meetingTimes)

    # Group the meeting times by section
    # Preserve the status and section info so it can be copied to meeting times that have left them blank (which is how UBC indicates that they are part of the same section).
    # Filter out sections based on status here
    persistentStatus = None
    persistentSection = None

    def getSectionNameAndPreserveInfo(meetingTime):
        # Either use and preserve new info, or use preserved info

        nonlocal persistentStatus
        nonlocal persistentSection

        section = meetingTime['section'].find('a')
        if section == None:
            status = persistentStatus
            section = persistentSection
        else:
            status = meetingTime['status'].getText()
            section = section.getText()

            persistentStatus = status
            persistentSection = section

        # Decide whether we should include this section
        if status in DISALLOWED_STATUSES:
            return None
        else:
            return section

    def applyPreservedInfo(meetingTime):
        meetingTime['status'] = persistentStatus
        meetingTime['section'] = persistentSection
        return meetingTime

    for activity in result.keys():
        result[activity] = group(
            getSectionNameAndPreserveInfo,
            result[activity],
            mapFunction=applyPreservedInfo
        )

    # Convert the bottom-level meeting times into the appropriate data format
    def convert(meetingTime):
        result = {
            'term': meetingTime['term'],
            'restricted': meetingTime['status'].strip() == 'Restricted',
            'mode': meetingTime['modeOfDelivery'].getText().strip()
        }

        try:
            result['comment'] = meetingTime['comments'].find_all('p')[
                0].getText().strip()
        except:
            result['comment'] = ''

        def convertTime(givenTime):
            hours, minutes = givenTime.getText().replace(' ', '').split(':')
            return {
                'sun': 0,
                'mon': 86400,
                'tue': 172800,
                'wed': 259200,
                'thu': 345600,
                'fri': 432000,
                'sat': 518400
            }[meetingTime['days'].lower()] + int(hours) * 3600 + int(minutes) * 60

        result['start'], result['end'] = convertTime(
            meetingTime['startTime']), convertTime(meetingTime['endTime'])

        if result['end'] < result['start']:
            result['end'] += 604800

        return result

    for activityKey in result.keys():
        for sectionKey in result[activityKey].keys():
            convertedMeetingTimes = []
            meetingTimeQueue = result[activityKey][sectionKey]

            # Doing it this way rather than with just a for loop is necessary because I'm splitting multi-term classes and multi-day classes into separate meeting times, for simplicity of the solver code
            while len(meetingTimeQueue):
                meetingTime, meetingTimeQueue = meetingTimeQueue[0], meetingTimeQueue[1:]

                if type(meetingTime['term']) is int:
                    pass
                elif meetingTime['term'].getText() in FULL_YEAR_TERM_VALUES:
                    term1 = copy.copy(meetingTime)
                    term1['term'] = 1
                    meetingTimeQueue.append(term1)
                    term2 = copy.copy(meetingTime)
                    term2['term'] = 2
                    meetingTimeQueue.append(term2)
                    continue
                else:
                    try:
                        meetingTime['term'] = int(
                            meetingTime['term'].getText())
                    except ValueError as e:
                        meetingTime['term'] = '1-2'

                if type(meetingTime['days']) is str:
                    pass
                else:
                    meetingTime['days'] = meetingTime['days'].getText().strip().split(
                        ' ')
                    if len(meetingTime['days']) > 1:
                        for day in meetingTime['days']:
                            newMeetingTime = copy.copy(meetingTime)
                            newMeetingTime['days'] = day
                            meetingTimeQueue.append(newMeetingTime)
                        continue
                    else:
                        meetingTime['days'] = meetingTime['days'][0]

                convertedMeetingTimes.append(convert(meetingTime))

            result[activityKey][sectionKey] = convertedMeetingTimes

    # Deduplicate meeting times
    for activityName in result.keys():
        for sectionName in result[activityName].keys():
            originalList = result[activityName][sectionName]
            newList = []

            for meetingTime in originalList:
                if meetingTime not in newList:
                    newList.append(meetingTime)

            result[activityName][sectionName] = newList

    return result


def getLink(classString):
    """ Returns the URL used to get section info.

    Parameters:
        classString (str): The human-readable name used to refer to a specific class or section

    Returns:
        The URL to get the info from.
    """
    try:
        subject, number, section = classString.split(' ', 2)
        return f'https://courses.students.ubc.ca/cs/courseschedule?pname=subjarea&tname=subj-section&dept={subject}&course={number}&section={section}'
    except ValueError:
        subject, number = classString.split(' ', 1)
        return f'https://courses.students.ubc.ca/cs/courseschedule?pname=subjarea&tname=subj-course&dept={subject}&course={number}'


def scrapeSection(sectionString: str) -> dict:
    """Scrape extra info about an individual section from UBC's website, that wasn't included when scraping the class

    Parameters:
        section string (str): A string identifying the section, in the form of `{subject} {class number} {section number}

    Returns:
        section (dict): {
            "credits": The number of credit hours the section is worth. Usually, all of them are attributed to the lecture and none are attributed to labs and tutorials
            "restriction": The rules that you must meet in order to register for the course if only restricted seats exist or remain
        }
    """

    html = rateLimit.request(getLink(sectionString), REQUEST_HEADERS)

    if bytes(COURSE_NOT_FOUND, 'utf-8') in html:
        raise ValueError('The section specified does not exist')

    html = BeautifulSoup(html, 'html.parser')

    # Get credit hours
    credits = 0
    paragraphs = html.find_all('p')
    for paragraph in paragraphs:
        text = paragraph.get_text().strip()
        if text.startswith('Credits: '):
            creditNumber = text.split(':')[1].strip()
            # If the portion after the colon is a valid integer, that's the number of credit hours the section is worth. If it is not a valid integer (such as n/a), then it's not worth any credits, probably because it's a lab/tutorial and all credits are attributed to the lecture.
            try:
                credits = int(creditNumber)
            except ValueError:
                credits = 0
            break

    # Get the restrictions
    tables = html.find_all('table')
    for table in tables:
        header = table.find('thead')
        if header == None:
            continue
        if header.get_text().strip() == 'Seat Summary':
            seatSummaryTable = table
            break

    # If there are 4 rows, there are no restrictions. If there are 5 rows, there is another table nested within the last one, with each row being a restriction
    rows = seatSummaryTable.find_all('tr', recursive=False)
    restrictions = ''
    if len(rows) == 5:
        restrictions = '\n'.join([row.get_text().strip()
                                  for row in rows[4].find_all('tr')])

    return {
        'credits': credits,
        'restriction': restrictions
    }


def _scrapeClassInternal(classString: str) -> dict:
    """Scrape info about a class from UBC's website

    Method for internal use, to enable code reuse between scrapeClass and fastUpdate. Only scrapes the class info page, not the section info pages.

    Parameters:
        classString (str): A string identifying a class, in the form of '{subject} {number}'

    Returns:
        _class (dict): A class in the data format described in main.py, except with missing credit hours and restrictions
    """

    html = rateLimit.request(getLink(classString), REQUEST_HEADERS)

    if bytes(COURSE_NOT_FOUND, 'utf-8') in html:
        raise ValueError('The course specified does not exist')

    meetTimesHTML = BeautifulSoup(html, 'html.parser').find('div', {'class': 'content expand'}).find(
        'table', {'class': 'table table-striped section-summary'})
    _class = _dictsToClass(
        [_meetTimeToDict(meetTime)
         for meetTime in meetTimesHTML.find_all('tr')[1:]],
        classString
    )

    return _class

# TODO make the return type hint better


def scrapeClass(classString: str) -> dict:
    """Scrape info about a class from UBC's website

    Parameters:
        classString (str): A string identifying a class, in the form of '{subject} {number}'

    Returns:
        _class (dict): A class in the data format described in main.py
    """

    while True:
        try:
            _class = _scrapeClassInternal(classString)

            # Scrape the section pages to get the credit hour and restriction information
            for activityName in _class.keys():
                for sectionName in _class[activityName].keys():
                    extraInfo = scrapeSection(sectionName)
                    for i in range(len(_class[activityName][sectionName])):
                        _class[activityName][sectionName][i] = {
                            **_class[activityName][sectionName][i],
                            **extraInfo
                        }

            return _class
        except:
            print(
                f'An error occurred. You may have triggered bot detection. Open {getLink(classString)} in your browser to complete the CAPTCHA if required.')
            if lib.promptYesNo('Retry?'):
                REQUEST_HEADERS['User-Agent'] = '-' + \
                    REQUEST_HEADERS['User-Agent']
                continue
            else:
                raise SystemExit()


def fastUpdate(classString: str, existingClass: dict) -> dict:
    """Scrape info about a class from UBC's website, preserving the already existing credit hour and restriction information.

    This is faster than updating everything, because getting the restriction information requires a request for every individual section. At a rate limit of 1 second per request, this can take upwards of a 30 seconds per class, which is unacceptable on registration day

    Parameters:
        classString (str): A string identifying a class, in the form of '{subject} {number}'
        existingClass (dict): The pre-existing class which credit hour and restriction data will be taken from

    Returns:
        _class (dict): A class in the data format described in main.py
    """

    _class = _scrapeClassInternal(classString)

    # Copy over existing information when possible
    for activityName in _class.keys():
        for sectionName in _class[activityName].keys():
            if sectionName in existingClass[activityName]:
                extraInfo = {
                    'credits': existingClass[activityName][sectionName][0]['credits'],
                    'restriction': existingClass[activityName][sectionName][0]['restriction']
                }
            else:
                extraInfo = scrapeSection(sectionName)

            for i in range(len(_class[activityName][sectionName])):
                _class[activityName][sectionName][i] = {
                    **_class[activityName][sectionName][i],
                    **extraInfo
                }

    return _class
