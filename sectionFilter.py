import os
import json
import xdg.BaseDirectory as xdg

CONFIG_PATH = os.path.join(xdg.xdg_config_home,
                           'classSched', 'sectionFilters.json')

DEFAULT_CONFIG = {
    # Whether waitlists are allowed. When allowed, they are converted into lecture sections.
    'allowWaitlist': False,

    # One of the following:
    #   'all': Allow all restricted sections
    #   'none': Disallow all restricted sections
    #   'custom': Allow or disallow restricted sections based on the rules below
    'allowRestricted': 'custom',

    # Allow all sections in one of these classes
    'allowedClasses': [],

    # Disallow all sections in one of these classes
    'disallowedClasses': [],

    # Allow all sections with one of these names
    'allowedSections': [],

    # Disallow all sections with one of these names
    'disallowedSections': [],

    # Allow all sections with one of these comments
    'allowedComments': [],

    # Disallow all sections with one of these comments
    'disallowedComments': [],

    'allowedRestrictions': [],
    'disallowedRestrictions': [],

    # Earliest allowed start time, as seconds since midnight. Or false to disable this filter.
    'dayStart': 32400,  # 9 AM
    # Latest allowed end time, as seconds since midnight. Or false to disable this filter.
    'dayEnd': 61200,    # 5 PM

    # The days when classes are allowed. The elements are true wwhen class is allowed on that day and false when it isn't. The elements represent the days of the week sequentially, with the week starting on Monday
    'allowedDays': [False, True, True, True, True, True, False],

    # Allowed modes of delivery
    'modes': {
        'In-Person': True,
        'Hybrid': False,
        'Online': False,
    },

    # Classes that are forced to be in a particular term
    'term1': [],
    'term2': [],

    # Sections that are exclusively allowed (in which case no other sections from the class activity are allowed) and sections that are disallowed, as specified by the user. They use a format similar to the overall class data format, except containing a list of section names rather than a dictionary of section names mapping to meeting times:
    # {
    #     className: {
    #         activityName: [
    #             sectionName,
    #             ...
    #         ],
    #         ...
    #     },
    #     ...
    # }
    'preferredSections': {},
    'unwantedSections': {}
}

if os.path.exists(CONFIG_PATH):
    try:
        with open(CONFIG_PATH) as file:
            config = json.JSONDecoder().decode(file.read())
    except json.JSONDecodeError:
        config = DEFAULT_CONFIG
else:
    config = DEFAULT_CONFIG


def getUndefinedFilters(classes):
    """ Given a list of classes, determine which restricted classes we don't yet know what to do with.

    If allowRestricted is all or none, this is always an empty list. If allowRestricted is custom, this is a list of dictionaries, where each dictionary represents a section that the user needs to be asked about.

    The dictionaries contain three keys: section, whose value represents the name of the section; class, whose value represents the class that the section goes with, and comment, whose value represents the comment associated with that section.

    Parameters:
        classes (dict): The classes as defined in main.py

    Returns:
        undefinedFilters (list): The list of sections that need to have filters created
    """

    if config['allowRestricted'] in ['all', 'none']:
        return []

    undefinedFilters = []

    for className in classes.keys():
        for activityName in classes[className].keys():
            for sectionName in classes[className][activityName].keys():
                comment = classes[className][activityName][sectionName][0]['comment']
                # If we have no rules relating to this section, we need to create one. If we have even one rule, we don't need to create one.
                if classes[className][activityName][sectionName][0]['restricted'] and (
                    className not in config['allowedClasses'] and
                    className not in config['disallowedClasses'] and
                    sectionName not in config['allowedSections'] and
                    sectionName not in config['disallowedSections'] and
                    comment not in config['allowedComments'] and
                    comment not in config['disallowedComments']
                ):
                    undefinedFilters.append({
                        'section': sectionName,
                        'class': className,
                        'comment': comment
                    })

    return undefinedFilters


def applyFilter(classes):
    """ Applies the section filter to the list of classes

    If waitlists are allowed, they are converted into lecture sections. If they are not allowed, they are deleted.

    If allowRestricted is set to all, all restricted sections are preserved without a check to see whether the user meets restrictions. Similarly, if set to none, all are removed without a check. If it is set to custom, these sections are preserved unless there is a rule requiring their removal.

    NOTE: Do not save the filtered classes to disk. The user might want to change the filters, and writing the filtered schedule to disk makes it necessary to refresh the classes every time the user wishes to do this. This is not guaranteed to be possible or desired.

    Parameters:
        classes (dict): Classes in the format described in main.py

    Returns:
        filtered (dict): Classes in the format described in main.py, but with the filters applied.
    """

    def printRemoval(section, reason):
        print(f'{section} removed: {reason}')

    def timeViolation(section):
        """ Returns whether the given section has a meeting time that starts too early, ends too late, or is on a prohibited day: reason or False """

        for meetTime in section:
            if meetTime['start'] % 86400 < config['dayStart']:
                return 'start time too early'
            elif meetTime['end'] % 86400 > config['dayEnd']:
                return 'end time too late'
            elif not config['allowedDays'][meetTime['start'] // 86400]:
                return 'prohibited day'

        return False

    filtered = {}

    for className in classes.keys():
        _class = {}
        for activityName in classes[className].keys():
            activity = {}   # Sections that we should add to the current activity
            # Sections that we should add to the lecture activity, regardless of their actual activity. (Currently used only for waitlists)
            addToLecture = {}
            for sectionName in classes[className][activityName].keys():

                # Skip restricted classes if necessary
                if classes[className][activityName][sectionName][0]['restricted']:
                    if config['allowRestricted'] == 'none':
                        printRemoval(
                            f'{className} {sectionName}', 'restricted')
                        continue
                    elif config['allowRestricted'] == 'custom' and (
                        sectionName in config['disallowedSections'] or
                        className in config['disallowedClasses'] or
                        classes[className][activityName][sectionName][0]['comment'] in config['disallowedComments']
                    ):
                        printRemoval(
                            f'{className} {sectionName}', 'restricted')
                        continue

                # If the class is a waitlist and we aren't allowing waitlists, skip it
                if (activityName == 'Waiting List' and not config['allowWaitlist']):
                    printRemoval(f'{className} {sectionName}', 'waitlist')
                    continue

                # If the class is not at an allowed time or on an allowed day, skip it
                violationReason = timeViolation(classes[className]
                                                [activityName][sectionName])
                if violationReason:
                    printRemoval(f'{className} {sectionName}', violationReason)
                    continue

                # If this class is not using an allowed delivery mode, skip it
                elif not config['modes'][classes[className][activityName][sectionName][0]['mode']]:
                    printRemoval(f'{className} {sectionName}', 'delivery mode')
                    continue

                # If the class has a preferred term, and this section isn't in the correct term, skip it
                elif (className in config['term1'] and classes[className][activityName][sectionName][0]['term'] != 1) or \
                        (className in config['term2'] and classes[className][activityName][sectionName][0]['term'] != 2):
                    printRemoval(f'{className} {sectionName}', 'wrong term')
                    continue

                # If the class activity has preferred sections and this isn't one of them, skip it
                elif (
                    className in config['preferredSections'].keys() and
                    activityName in config['preferredSections'][className].keys() and
                    sectionName not in config['preferredSections'][className][activityName]
                ):
                    printRemoval(f'{className} {sectionName}',
                                 'non-preferred section')
                    continue

                # If the class activity has disallowed sections and this is one of them, skip it
                elif (
                    className in config['unwantedSections'].keys() and
                    activityName in config['unwantedSections'][className].keys() and
                    sectionName in config['unwantedSections'][className][activityName]
                ):
                    printRemoval(f'{className} {sectionName}',
                                 'disallowed section section')
                    continue

                # If this activity is a waiting list and we're supposed to allow them (implied by the fact that we reached this point because they would have been eliminated earlier), add it to the lecture sections.
                elif activityName == 'Waiting List':
                    addToLecture[sectionName] = classes[className][activityName][sectionName]

                # If this activity is not a waiting list, add it to the appropriate activity
                else:
                    activity[sectionName] = classes[className][activityName][sectionName]

            # Store the data to be returned
            if len(activity.keys()) > 0:
                _class[activityName] = activity
            if len(addToLecture.keys()) > 0:
                _class['Lecture'] = {**_class['Lecture'], **addToLecture}
        if len(_class.keys()) > 0:
            filtered[className] = _class

    return filtered
